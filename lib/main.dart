import 'dart:math';

import 'package:flutter/material.dart';

final Random randProgress = new Random();

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
            child: CustomTile()
        ),
      ),
    );
  }
}

class CustomTile extends StatefulWidget {
  @override
  CustomTileState createState() => CustomTileState();
}

class CustomTileState extends State<CustomTile> {
  Color color;

  @override
  void initState() {
    super.initState();

    color = Colors.transparent;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: color,
      child: ListTile(

        title:  new Center(child: new Text("Hey there",
          style: new TextStyle(
              fontWeight: FontWeight.w500, fontSize: 25.0),)),
        onTap: () {
          setState(() {
            color = _getRandomColor();
          });
        },
      ),
    );
  }

  Color _getRandomColor() {
    List<MaterialColor> primaries = Colors.primaries;
    int colorProgress = randProgress.nextInt(primaries.length);
    return primaries.elementAt(colorProgress);
  }
}